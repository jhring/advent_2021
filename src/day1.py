import numpy as np


def day1(depths: np.array = None) -> (int, int):
    if depths is None:
        depths = np.loadtxt("../data/day1.txt", dtype=int)
    sums = np.array([sum(depths[idx:idx + 3]) for idx in range(len(depths) - 2)])  # could use np.convolve
    return np.sum(depths[1:] > depths[:-1]), np.sum(sums[1:] > sums[:-1])


if __name__ == '__main__':
    p1, p2 = day1()
    print("Part 1:", p1)
    print("Part 2:", p2)
