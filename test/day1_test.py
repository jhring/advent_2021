import numpy as np
from src.day1 import day1


def test_p1():
    data = np.array([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
    p1, _ = day1(data)
    assert 7 == p1


def test_p2():
    data = np.array([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
    _, p2 = day1(data)
    assert 5 == p2


def test_p1_sol():
    p1, _ = day1()
    assert p1 == 1616


def test_p2_sol():
    _, p2 = day1()
    assert p2 == 1645
